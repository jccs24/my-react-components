import React from "react"

export default function CoolList(props) {

    return (
        <ul className="w-2/3 m-auto border-2 border-red-200 rounded-md p-2 mb-4">
            {props.elements.map((value, index) => <li className="w-auto border-b-2 flex items-center justify-between last:border-0" key={index}><div>{value}</div> {props.borrar ? <button className="bg-gray-300 my-1 p-1 px-2" onClick={() => props.borrarElemento(value)}>&times;</button> : ''}</li>)}
        </ul>
    )
}