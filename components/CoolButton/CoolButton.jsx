import React from "react"

export default function CoolButton(props){
    return (
        <button onClick={props.clickAction} className="p-2 rounded-md bg-rose-800 text-white disabled:opacity-75 disabled:bg-gray-100 disabled:text-gray-400 cursor-pointer">{props.text ? props.text : 'Cool button...'}</button>
    )
}